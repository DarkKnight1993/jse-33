package ru.tsc.goloshchapov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.api.repository.IProjectRepository;
import ru.tsc.goloshchapov.tm.enumerated.Status;
import ru.tsc.goloshchapov.tm.model.Project;

import java.util.Date;
import java.util.Optional;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @Nullable
    @Override
    public Project findByName(@NotNull final String userId, @NotNull final String name) {
        return entities.stream()
                .filter(u -> userId.equals(u.getUserId()))
                .filter(p -> name.equals(p.getName()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public Project removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Optional<Project> project = Optional.ofNullable(findByName(userId, name));
        project.ifPresent(this::remove);
        return project.orElse(null);
    }

    @Nullable
    @Override
    public Project startById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Optional<Project> project = Optional.ofNullable(findById(userId, id));
        project.ifPresent(p -> {
            p.setStatus(Status.IN_PROGRESS);
            p.setStartDate(new Date());
        });
        return project.orElse(null);
    }

    @Nullable
    @Override
    public Project startByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Optional<Project> project = Optional.ofNullable(findByIndex(userId, index));
        project.ifPresent(p -> {
            p.setStatus(Status.IN_PROGRESS);
            p.setStartDate(new Date());
        });
        return project.orElse(null);
    }

    @Nullable
    @Override
    public Project startByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Optional<Project> project = Optional.ofNullable(findByName(userId, name));
        project.ifPresent(p -> {
            p.setStatus(Status.IN_PROGRESS);
            p.setStartDate(new Date());
        });
        return project.orElse(null);
    }

    @Nullable
    @Override
    public Project finishById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Optional<Project> project = Optional.ofNullable(findById(userId, id));
        project.ifPresent(p -> {
            p.setStatus(Status.COMPLETED);
            p.setFinishDate(new Date());
        });
        return project.orElse(null);
    }

    @Nullable
    @Override
    public Project finishByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Optional<Project> project = Optional.ofNullable(findByIndex(userId, index));
        project.ifPresent(p -> {
            p.setStatus(Status.COMPLETED);
            p.setFinishDate(new Date());
        });
        return project.orElse(null);
    }

    @Nullable
    @Override
    public Project finishByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Optional<Project> project = Optional.ofNullable(findByName(userId, name));
        project.ifPresent(p -> {
            p.setStatus(Status.COMPLETED);
            p.setFinishDate(new Date());
        });
        return project.orElse(null);
    }

    @Nullable
    @Override
    public Project changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        @NotNull final Optional<Project> project = Optional.ofNullable(findById(userId, id));
        project.ifPresent(p -> {
            p.setStatus(status);
            if (status == Status.IN_PROGRESS) p.setStartDate(new Date());
            if (status == Status.COMPLETED) p.setFinishDate(new Date());
        });
        return project.orElse(null);
    }

    @Nullable
    @Override
    public Project changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        @NotNull final Optional<Project> project = Optional.ofNullable(findByIndex(userId, index));
        project.ifPresent(p -> {
            p.setStatus(status);
            if (status == Status.IN_PROGRESS) p.setStartDate(new Date());
            if (status == Status.COMPLETED) p.setFinishDate(new Date());
        });
        return project.orElse(null);
    }

    @Nullable
    @Override
    public Project changeStatusByName(@NotNull final String userId, @NotNull final String name, @NotNull final Status status) {
        @NotNull final Optional<Project> project = Optional.ofNullable(findByName(userId, name));
        project.ifPresent(p -> {
            p.setStatus(status);
            if (status == Status.IN_PROGRESS) p.setStartDate(new Date());
            if (status == Status.COMPLETED) p.setFinishDate(new Date());
        });
        return project.orElse(null);
    }

}
