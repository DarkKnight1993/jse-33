package ru.tsc.goloshchapov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Getter
@Setter
public abstract class AbstractOwnerEntity extends AbstractEntity implements Serializable {

    @NotNull
    protected String userId;

}
