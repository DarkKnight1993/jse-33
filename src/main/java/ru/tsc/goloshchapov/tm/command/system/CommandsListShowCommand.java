package ru.tsc.goloshchapov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.goloshchapov.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandsListShowCommand extends AbstractSystemCommand {
    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list of commands";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        @NotNull final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands) showCommandValue(command.name());
    }

}
