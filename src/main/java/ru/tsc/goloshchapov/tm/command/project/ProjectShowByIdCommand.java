package ru.tsc.goloshchapov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.enumerated.Role;
import ru.tsc.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.goloshchapov.tm.model.Project;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {
    @NotNull
    @Override
    public String name() {
        return "project-show-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by id";
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW PROJECT INFO BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectService().findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
