package ru.tsc.goloshchapov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.goloshchapov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ArgumentsListShowCommand extends AbstractSystemCommand {
    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list of arguments";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getArguments();
        for (final AbstractCommand command : commands) showCommandValue(command.arg());
    }

}
