package ru.tsc.goloshchapov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.dto.Domain;

import java.io.FileOutputStream;

public class DataJsonSaveFasterXmlCommand extends AbstractDataCommand {
    @Override
    public @NotNull String name() {
        return "data-save-json";
    }

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String description() {
        return "Save data in json format by fasterxml";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA JSON SAVE BY FASTERXML]");
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTERXML_JSON);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
        System.out.println("[OK]");
    }

}
