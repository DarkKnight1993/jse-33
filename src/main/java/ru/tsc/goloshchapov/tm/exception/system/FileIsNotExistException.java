package ru.tsc.goloshchapov.tm.exception.system;

import ru.tsc.goloshchapov.tm.exception.AbstractException;

public class FileIsNotExistException extends AbstractException {

    public FileIsNotExistException() {
        super("Exception! File is not exist! ");
    }

    public FileIsNotExistException(final String value) {
        super("Exception! File with name `" + value + "` is not exist!");
    }

    public FileIsNotExistException(final Throwable cause) {
        super(cause);
    }

}
