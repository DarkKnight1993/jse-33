package ru.tsc.goloshchapov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.tsc.goloshchapov.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class FileScanner {

    @NotNull
    private static final String PATH = "./";

    private static final int INTERVAL = 10000;

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final Bootstrap bootstrap;

    public FileScanner(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        commands.addAll(bootstrap.getCommandService().getArguments().stream().map(AbstractCommand::name).collect(Collectors.toList()));
        executorService.scheduleWithFixedDelay(this::run, 0, INTERVAL, TimeUnit.MILLISECONDS);
    }

    public void run() {
        @NotNull final File file = new File(PATH);
        Arrays.stream(file.listFiles())
                .filter(o -> o.isFile() && commands.contains(getFileNameWithoutExtension(o.getName())))
                .forEach(o -> {
                    System.out.println("COMMAND " + getFileNameWithoutExtension(o.getName()) + " IS LAUNCHED BY FILE");
                    bootstrap.runCommand(getFileNameWithoutExtension(o.getName()));
                    o.delete();
                });
    }

    public void stop() {
        executorService.shutdown();
    }

    @NotNull
    private String getFileNameWithoutExtension(String fileName) {
        if (fileName.indexOf(".") > 0) {
            return fileName.substring(0, fileName.lastIndexOf("."));
        } else {
            return fileName;
        }
    }

}
